package main.java.teht_02;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Main {

	public static void main(String[] args) {
		
		 // Alustukset
		
		Koodaaja jasper = null;
		Class c = null;
		VaateFactory tehdas = null;
		Properties properties = new Properties();
		
		
		 // Ladataan "tehdas.properties" properties muuttujaan
		 
		  try {
		    properties.load(
		          new FileInputStream("src/main/java/teht_02/tehdas.properties"));
		  } catch (IOException e) {e.printStackTrace();}
		  try{
		    
		     // Luetaan Adidas tiedostosta
		    
		    c = Class.forName(properties.getProperty("adidas"));
		    tehdas = (VaateFactory)c.getDeclaredConstructor().newInstance();
		    
		    
		     // Jasper käyttää Adidas-tehdasta
		     
		    jasper = new Jasper(tehdas);
		    
		    
			 // Jasper kertoo mitä vaatetta on yllä
			 
			jasper.kerroFitti();
			
			
		     // Luetaan Boss tiedostosta
		     
		    c = Class.forName(properties.getProperty("boss"));
		    tehdas = (VaateFactory)c.getDeclaredConstructor().newInstance();
		    
		     // Jasper käyttää Boss-tehdasta
		     
		    jasper = new Jasper(tehdas);
		    
			 // Jasper kertoo uudestaa mitä on päällä
			 
			jasper.kerroFitti();
		  } catch (Exception e){e.printStackTrace();}
	}

}

