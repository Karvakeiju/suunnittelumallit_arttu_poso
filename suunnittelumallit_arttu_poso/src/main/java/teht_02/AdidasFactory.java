package main.java.teht_02;

public class AdidasFactory extends VaateFactory {

	@Override
	public Farkut createFarkut() {
		return new AdidasFarkut();
	}

	@Override
	public Tpaita createTpaita() {
		return new AdidasTpaita();
	}

	@Override
	public Lippis createLippis() {
		return new AdidasLippis();
	}

	@Override
	public Kengät createKenkä() {
		return new AdidasKengät();
	}

}
