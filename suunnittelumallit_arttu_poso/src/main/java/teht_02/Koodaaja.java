package main.java.teht_02;

public abstract class Koodaaja {
	private VaateFactory fitti = null;
	
	public Koodaaja(VaateFactory tehdas) {
		fitti = tehdas;
	}
	
	public void vaihdaUlkoasua(VaateFactory uusiFitti) {
		fitti = uusiFitti;
	}
	
	public void kerroFitti() {
		fitti.vaatteet();
	}
}
