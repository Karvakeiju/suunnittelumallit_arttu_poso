package main.java.teht_02;

public class BossFactory extends VaateFactory {

	@Override
	public Farkut createFarkut() {
		return new BossFarkut();
	}

	@Override
	public Tpaita createTpaita() {
		return new BossTpaita();
	}

	@Override
	public Lippis createLippis() {
		return new BossLippis();
	}

	@Override
	public Kengät createKenkä() {
		return new BossKengät();
	}

}
