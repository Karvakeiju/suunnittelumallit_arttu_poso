package main.java.teht_02;

public abstract class VaateFactory {
	
	private Farkut farkut = null;
	private Kengät kengät = null;
	private Lippis lippis = null;
	private Tpaita tpaita = null;

	
	public abstract Farkut createFarkut();
	
	public abstract Tpaita createTpaita();
	
	public abstract Lippis createLippis();
	
	public abstract Kengät createKenkä();
	
	public void vaatteet() {
		fitti();
		kerroFitti();
	}
	
	private void kerroFitti() {
		System.out.println("Mul on päällä päheet " + farkut + ", " + tpaita + ", " + kengät + " ja " + lippis + ".");
	}
	
	
	private void fitti() {
		if(farkut==null)
			farkut = createFarkut();
		if(kengät==null)
			kengät = createKenkä();
		if(lippis==null)
			lippis = createLippis();
		if(tpaita==null)
			tpaita = createTpaita();

	}
	

}
