package main.java.teht_11;

import java.util.Random;

public class Arvuuttaja {
	public void liityPeliin(Asiakas pelaaja) {
		int luku = new Random().nextInt(101);
		System.out.println("Pelaaja-" + pelaaja.getNimi() + " sai luvukseen: " + luku);
		pelaaja.setLuku(new Memento(luku));
	}
	
	public boolean arvaaLuku(Asiakas pelaaja) {
		try {
			Memento memento = (Memento) pelaaja.getLuku();
			int pelaajaArvaa = pelaaja.arvaaLuku();
			System.out.println(pelaaja.getNimi() + " arvasi lukua: " + pelaajaArvaa + " ("+ memento.luku +")");
			if (memento.luku==pelaajaArvaa) {
				System.out.println("Arvaus oli oikein.");
				return true;
			}
		} catch (Exception e) {
			System.out.println("Pelaaja yritti arvata liittymättä peliin.");
		}
		System.out.println("Arvaus oli väärin");
		return false;

	}
	
	private class Memento {
		private int luku;
		public Memento(int luku) {
			this.luku = luku;
		}

	}
}
