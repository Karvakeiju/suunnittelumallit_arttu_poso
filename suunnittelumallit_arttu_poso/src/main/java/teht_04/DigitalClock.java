package main.java.teht_04;


public class DigitalClock extends Observer {

	private ClockTimer timer;
	
	public DigitalClock(ClockTimer ct){
		timer = ct;
		timer.attach(this);
	}
	
	private void draw(){
		int h = timer.getHour();
		int min = timer.getMinute();
		int sec = timer.getSecond();
		System.out.println(h+":"+min+":"+sec);
	}
	
	@Override
	public void update(Subject s) {
		if (s == timer){
			draw();
		}
	}
}

