package main.java.teht_06;

public class Main {

	public static void main(String[] args) {
		
		 // Tehdään uusi Otto-automaatti, jossa on tili "1337"
		 
		Pankki ottoAutomaatti = new Ottoautomaatti(new Pankkitili(1337));
		

		System.out.println("-Katsotaan tilin saldo oikealla pin-koodilla-");
		System.out.println(ottoAutomaatti.saldo(1337).getViesti());
		
		System.out.println("-Katsotaan tilin saldo väärällä pin-koodilla-");
		System.out.println(ottoAutomaatti.saldo(1234).getViesti());
		
		System.out.println("-Tehdään talletus oikealla pin-koodilla-");
		System.out.println(ottoAutomaatti.talleta(1337, 99.25).getViesti());
		
		System.out.println("-Katsotaan tilin saldo oikealla pin-koodilla-");
		System.out.println(ottoAutomaatti.saldo(1337).getViesti());
		
		System.out.println("-Tehdään nosto tililtä väärällä pin-koodilla-");
		System.out.println(ottoAutomaatti.nosta(1234, 10.10).getViesti());
		
		System.out.println("-Tehdään nosto oikealla pin-koodilla-");
		System.out.println(ottoAutomaatti.nosta(1337, 25.25).getViesti());
		
		System.out.println("-Tehdään tilin katteen ylittävä nosto oikealla pin-koodilla-");
		System.out.println(ottoAutomaatti.nosta(1337, 225.15).getViesti());

	}

}
