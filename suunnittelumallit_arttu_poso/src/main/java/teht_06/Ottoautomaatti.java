package main.java.teht_06;

public class Ottoautomaatti extends Pankkiautomaatti{
	
	public Ottoautomaatti(Pankki pankki) {
		super(pankki);
	}
	
	@Override
	public Viesti saldo(int pin) {
		return super.saldo(pin);
	}

	@Override
	public Viesti talleta(int pin, double maara) {
		return super.talleta(pin, maara);
	}

	@Override
	public Viesti nosta(int pin, double maara) {
		return super.nosta(pin, maara);
	}




}
