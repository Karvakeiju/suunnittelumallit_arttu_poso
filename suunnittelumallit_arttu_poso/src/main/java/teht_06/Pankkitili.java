package main.java.teht_06;

public class Pankkitili implements Pankki {
	
	protected int pin;
	protected double saldo;
	
	public Pankkitili(int pin) {
		this.pin = pin;
		saldo = 0;
	}

	@Override
	public Viesti saldo(int pin) {
		Viesti viesti = new Viesti();
		if(pin == this.pin) {
			viesti.pinOk();
			viesti.setSaldo(saldo);
			return viesti;
		}
		viesti.pinVäärä();
		return viesti;
	}

	@Override
	public Viesti talleta(int pin, double määrä) {
		Viesti viesti = new Viesti();
		if(pin == this.pin) {
			viesti.pinOk();
			if(määrä>0)
				saldo+=määrä;
			return viesti;
		}
		viesti.pinVäärä();
		return viesti;
	}

	@Override
	public Viesti nosta(int pin, double määrä) {
		Viesti viesti = new Viesti();
		if(pin == this.pin) {
			viesti.pinOk();
			if(määrä <= saldo) {
				saldo-=määrä;
				viesti.nostaRaha(määrä);
			} else {
				viesti.alhainenSaldo();
			}
			return viesti;
		}
		viesti.pinVäärä();
		return viesti;
	}
}
