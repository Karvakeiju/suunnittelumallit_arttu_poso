package main.java.teht_06;

public class Viesti {
	private String viesti;
	protected double saldo;
	
	public Viesti() {
		viesti = "";
		saldo = 0;
	}
	
	public void pinOk() {
		viesti += "Pin-koodi ok.\n";
	}
	
	public void pinVäärä() {
		viesti += "Pin-koodi virheellinen.\n";
	}
	
	public void alhainenSaldo() {
		viesti += "Tilin saldo ei ole riittävä.\n";
	}
	
	public void nostaRaha(double määrä) {
		viesti += "Tililtä nostettiin " + määrä + " euroa.\n";
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		viesti += "Saldo: " + saldo + " euroa.\n";
	}
	
	public String getViesti() {
		return viesti;
	}

}
