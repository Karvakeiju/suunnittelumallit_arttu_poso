package main.java.teht_22;

public class RollUpCommand implements Command {
	private Screen screen;
	
	public RollUpCommand(Screen screen) {
		this.screen = screen;
	}

	@Override
	public void execute() {
		screen.rollUp();
	}
}

