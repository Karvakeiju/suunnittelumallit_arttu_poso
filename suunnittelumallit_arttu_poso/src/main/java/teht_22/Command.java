package main.java.teht_22;

public interface Command {
	public void execute();
}
