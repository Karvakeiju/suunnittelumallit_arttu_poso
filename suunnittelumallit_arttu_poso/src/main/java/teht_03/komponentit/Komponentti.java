package main.java.teht_03.komponentit;

import java.util.HashSet;
import java.util.Set;

public abstract class Komponentti {
	
	 // Käytetään Settiä, niin ei tule duplikaatteja
	 
	private Set<Komponentti> komponentit = new HashSet<Komponentti>();
	private double hinta = 0;
	
	public abstract void komponentinKuvaus();
	
	public Komponentti(double hinta) {
		this.hinta = hinta;
	}
	
	public void setHinta(double hinta) {
		this.hinta = hinta;
	}
		
	public double getHinta() {
		double summa = 0;
		for(Komponentti k : komponentit) {
			summa+= k.getHinta();
		}
		return Math.round((summa+hinta) * 100.0) / 100.0;
	}
	
	public void lisääKomponentti(Komponentti k) {
		komponentit.add(k);
	}
	
	public void poistaKomponentti(Komponentti k) {
		komponentit.remove(k);
	}
	
	public void listaaKomponentit() {
		for(Komponentti k : komponentit)
			k.listaaKomponentit();
		komponentinKuvaus();
	}
	

}
