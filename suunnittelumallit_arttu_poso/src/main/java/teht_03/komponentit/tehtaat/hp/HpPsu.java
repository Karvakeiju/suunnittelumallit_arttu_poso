package main.java.teht_03.komponentit.tehtaat.hp;
import main.java.teht_03.komponentit.Virtalähde;

public class HpPsu extends Virtalähde {

	public HpPsu() {
		super(189.99);
	}
	
	public HpPsu(int hinta) {
		super(hinta);
	}

	@Override
	public void komponentinKuvaus() {
		System.out.println("Virtalähde: tehokas virtalähde");
		
	}

}