package main.java.teht_03.komponentit.tehtaat.hp;
import main.java.teht_03.komponentit.Koppa;

public class HpCase extends Koppa{

	public HpCase() {
		super(49.99);
	}
	
	public HpCase(int hinta) {
		super(hinta);
	}

	@Override
	public void komponentinKuvaus() {
		System.out.println("Koppa: tosi tyylikäs edullinen koppa");
		
	}
}
