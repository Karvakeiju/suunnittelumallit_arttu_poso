package main.java.teht_03.komponentit.tehtaat.hp;

import main.java.teht_03.komponentit.Komponentti;
import main.java.teht_03.komponentit.tehtaat.*;

public class HpTehdas extends KokoonpanoTehdas {

	@Override
	public Komponentti haeKokoonpano() {
		
		 // Aloitetaan Hewlett-Packard-kokoonpano kopasta ja lisätään siihen komponentti kerrallaan, kunnes saadaan kokonainen tietokone
			
		Komponentti kokoonpano = new HpCase();
		

		Komponentti emolevy = new HpMobo();
		

		emolevy.lisääKomponentti(new HpCpu());
		

		emolevy.lisääKomponentti(new HpGpu());
		
		// Lisätään kaksi kappaletta välimuistitikkuja, sillä niitä harvoin on vain yksi.
		
		emolevy.lisääKomponentti(new HpRam());
		emolevy.lisääKomponentti(new HpRam());
		

		emolevy.lisääKomponentti(new HpExtcard());
		

		kokoonpano.lisääKomponentti(emolevy);
		

		kokoonpano.lisääKomponentti(new HpPsu());
		
		return kokoonpano;
	}

}