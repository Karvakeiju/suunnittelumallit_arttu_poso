package main.java.teht_03.komponentit.tehtaat.hp;
import main.java.teht_03.komponentit.Emolevy;

public class HpMobo extends Emolevy {

	public HpMobo() {
		super(199.99);
	}
	
	public HpMobo(int hinta) {
		super(hinta);
	}

	@Override
	public void komponentinKuvaus() {
		System.out.println("Emolevy: hyvä peruskelpo emolevy");
		
	}

}
