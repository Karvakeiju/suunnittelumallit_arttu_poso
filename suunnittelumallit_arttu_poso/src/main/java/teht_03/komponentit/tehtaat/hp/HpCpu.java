package main.java.teht_03.komponentit.tehtaat.hp;
import main.java.teht_03.komponentit.Prosessori;

public class HpCpu extends Prosessori {

	public HpCpu() {
		super(199.99);
	}
	
	public HpCpu(int hinta) {
		super(hinta);
	}

	@Override
	public void komponentinKuvaus() {
		System.out.println("Prosessori: Intel i7 -tehoprossu");
		
	}

}
