package main.java.teht_03.komponentit.tehtaat.hp;
import main.java.teht_03.komponentit.Näytönohjain;

public class HpGpu extends Näytönohjain {

	public HpGpu() {
		super(499.99);
	}
	
	public HpGpu(int hinta) {
		super(hinta);
	}

	@Override
	public void komponentinKuvaus() {
		System.out.println("GPU: Kallis Nvidia GEFORCE RTX 3060");
		
	}

}

