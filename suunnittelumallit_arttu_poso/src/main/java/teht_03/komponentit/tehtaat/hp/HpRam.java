package main.java.teht_03.komponentit.tehtaat.hp;
import main.java.teht_03.komponentit.Välimuisti;

public class HpRam extends Välimuisti {

	public HpRam() {
		super(199.99);
	}
	
	public HpRam(int hinta) {
		super(hinta);
	}

	@Override
	public void komponentinKuvaus() {
		System.out.println("RAM: G.Skillin 3200mHz 8Gb -tikku");
		
	}

}
