package main.java.teht_03.komponentit.tehtaat.hp;
import main.java.teht_03.komponentit.Lisäkortti;

public class HpExtcard extends Lisäkortti {

	public HpExtcard() {
		super(325.99);
	}
	
	public HpExtcard(int hinta) {
		super(hinta);
	}

	@Override
	public void komponentinKuvaus() {
		System.out.println("Lisäkortti: Intelin täyden gigan verkkokortti");
		
	}
}