package main.java.teht_03.komponentit.tehtaat.apple;
import main.java.teht_03.komponentit.Emolevy;

public class AppleMobo extends Emolevy {

	public AppleMobo() {
		super(299.99);
	}
	
	public AppleMobo(int hinta) {
		super(hinta);
	}

	@Override
	public void komponentinKuvaus() {
		System.out.println("Emolevy: ei ollenkaan päivittämismahdollisuutta");
		
	}

}
