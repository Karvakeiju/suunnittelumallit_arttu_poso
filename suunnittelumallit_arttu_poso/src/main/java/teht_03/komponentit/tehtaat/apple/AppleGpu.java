package main.java.teht_03.komponentit.tehtaat.apple;
import main.java.teht_03.komponentit.Näytönohjain;

public class AppleGpu extends Näytönohjain {

	public AppleGpu() {
		super(599.99);
	}
	
	public AppleGpu(int hinta) {
		super(hinta);
	}

	@Override
	public void komponentinKuvaus() {
		System.out.println("GPU: Todella ylihinnoiteltu Nvidian kortti");
		
	}

}
