package main.java.teht_03.komponentit.tehtaat.apple;
import main.java.teht_03.komponentit.Välimuisti;

public class AppleRam extends Välimuisti {

	public AppleRam() {
		super(199.99);
	}
	
	public AppleRam(int hinta) {
		super(hinta);
	}

	@Override
	public void komponentinKuvaus() {
		System.out.println("RAM: G.Skillin 3200mHz 8Gb -tikku");
		
	}

}

