package main.java.teht_03.komponentit.tehtaat.apple;
import main.java.teht_03.komponentit.Virtalähde;

public class ApplePsu extends Virtalähde {

	public ApplePsu() {
		super(189.99);
	}
	
	public ApplePsu(int hinta) {
		super(hinta);
	}

	@Override
	public void komponentinKuvaus() {
		System.out.println("Virtalähde: tehokas virtalähde");
		
	}

}
