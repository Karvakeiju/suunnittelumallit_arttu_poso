package main.java.teht_03.komponentit.tehtaat.apple;
import main.java.teht_03.komponentit.Prosessori;

public class AppleCpu extends Prosessori {

	public AppleCpu() {
		super(399.99);
	}
	
	public AppleCpu(int hinta) {
		super(hinta);
	}

	@Override
	public void komponentinKuvaus() {
		System.out.println("Prosessori: Intelin ylihinnoiteltu prossu");
		
	}

}
