package main.java.teht_03.komponentit.tehtaat.apple;
import main.java.teht_03.komponentit.Lisäkortti;

public class AppleExtcard extends Lisäkortti {

	public AppleExtcard() {
		super(385.99);
	}
	
	public AppleExtcard(int hinta) {
		super(hinta);
	}

	@Override
	public void komponentinKuvaus() {
		System.out.println("Lisäkortti: Intelin täyden gigan verkkokortti");
		
	}
}




