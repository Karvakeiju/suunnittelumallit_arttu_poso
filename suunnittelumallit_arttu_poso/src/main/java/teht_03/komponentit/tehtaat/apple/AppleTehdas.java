package main.java.teht_03.komponentit.tehtaat.apple;

import main.java.teht_03.komponentit.Komponentti;
import main.java.teht_03.komponentit.tehtaat.*;

public class AppleTehdas extends KokoonpanoTehdas {

	@Override
	public Komponentti haeKokoonpano() {
		
		 // Aloitetaan Apple-kokoonpano kopasta ja lisätään siihen komponentti kerrallaan, kunnes saadaan kokonainen tietokone
			
		Komponentti kokoonpano = new AppleCase();
		

		Komponentti emolevy = new AppleMobo();
		

		emolevy.lisääKomponentti(new AppleCpu());
		

		emolevy.lisääKomponentti(new AppleGpu());
		
		// Lisätään kaksi kappaletta välimuistitikkuja, sillä niitä harvoin on vain yksi.
		
		emolevy.lisääKomponentti(new AppleRam());
		emolevy.lisääKomponentti(new AppleRam());
		

		emolevy.lisääKomponentti(new AppleExtcard());
		

		kokoonpano.lisääKomponentti(emolevy);
		

		kokoonpano.lisääKomponentti(new ApplePsu());
		
		return kokoonpano;
	}

}
