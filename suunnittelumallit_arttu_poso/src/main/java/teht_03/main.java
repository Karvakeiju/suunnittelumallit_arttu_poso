package main.java.teht_03;

import main.java.teht_03.komponentit.tehtaat.KokoonpanoTehdas;

public class main {

	public static void main(String[] args) {
		double summa1 = 0;
		double summa2 = 0;
		
		
		 // Haetaan ensiksi edullinen Hewlett-Packard tehdas
		  
		 // Käytetään Reflection API:a. 
		 
		KokoonpanoTehdas tehdas = HaeOminaisuuksista.haeHp();
		
		
		 // Varmistetaan ettei tehdas ole null
		 
		if(tehdas!=null) {
			System.out.println("-----Hewlett-Packard tehtaan kokoonpano-----");
			tehdas.listaaKokoonpano();
			summa1 = tehdas.getKokoonpanonHinta();
		}
		
		
		 // Varmistetaan ettei tehdas ole null
		 
		tehdas = HaeOminaisuuksista.haeApple();
		if(tehdas!=null) {
			System.out.println("\n-----Apple tehtaan kokoonpano-----");
			tehdas.listaaKokoonpano();
			summa2 = tehdas.getKokoonpanonHinta();
		}
		System.out.println("\nHewlett-Packardin kokoonpanon hinta: "+summa1
				+"€\nApplen kokoonpanon hinta:            "+summa2+"€");

	}

}
