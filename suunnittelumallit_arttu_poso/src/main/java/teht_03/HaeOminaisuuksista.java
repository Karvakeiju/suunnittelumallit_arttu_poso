package main.java.teht_03;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import main.java.teht_03.komponentit.tehtaat.KokoonpanoTehdas;

public class HaeOminaisuuksista {
	public static KokoonpanoTehdas haeHp() {
		KokoonpanoTehdas tehdas;
		Properties properties = new Properties();
				
				
				 // Ladataan "tehdas.properties" properties muuttujaan
				 
				  try {
				    properties.load(
				          new FileInputStream("src/main/java/teht_03/komponentit/tehtaat/tehdas.properties"));
				  } catch (IOException e) {e.printStackTrace();}
				  try{
				    
				     // Luetaan filusta Hewlett-Packard tehdas
				     
					Class c = Class.forName(properties.getProperty("hp"));
				    tehdas = (KokoonpanoTehdas)c.getDeclaredConstructor().newInstance();
				    				    
				     // Jos tehdas löytyy, palautetaan se
				     
				    return tehdas;
				  } catch (Exception e){e.printStackTrace();}
				  
				   // Jos vastaavaa tehdasta ei löydetä, palautetaan null
				   
				  return null;
	}
	
	public static KokoonpanoTehdas haeApple() {
		KokoonpanoTehdas tehdas;
		Properties properties = new Properties();
				
				
				 // Ladataan "tehdas.properties" properties muuttujaan
				 
				  try {
				    properties.load(
				          new FileInputStream("src/main/java/teht_03/komponentit/tehtaat/tehdas.properties"));
				  } catch (IOException e) {e.printStackTrace();}
				  try{
				    
				     // Luetaan filusta Apple tehdas
				     
				    Class c = Class.forName(properties.getProperty("apple"));
				    tehdas = (KokoonpanoTehdas)c.getDeclaredConstructor().newInstance();
				    				    
				     // Jos tehdas löytyy, palautetaan se
				     
				    return tehdas;
				  } catch (Exception e){e.printStackTrace();}
				  
				  
				  // Jos vastaavaa tehdasta ei löydetä, palautetaan null
				   
				  return null;
	}
	
}

