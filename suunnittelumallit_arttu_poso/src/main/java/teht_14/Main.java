package main.java.teht_14;

public class Main {

	public static void main(String[] args) {
		HesburgerBuilder HR = new HesburgerBuilder();
		McDonaldsBuilder MDR = new McDonaldsBuilder();
		Kokki kokki = new Kokki();
		
		System.out.println("--HESEN KOKKI TEKEE BURGERIN");
		kokki.asetaRakentaja(HR);
		kokki.rakennaBurgeri();
		HesburgerBurger HBurgeri = (HesburgerBurger) HR.haeHampurilainen();
		System.out.println(HBurgeri);
		
		System.out.println("\n--MÄKIN KOKKI TEKEE BURGERIN");
		kokki.asetaRakentaja(MDR);
		kokki.rakennaBurgeri();
		McDonaldsBurger MBurgeri = (McDonaldsBurger) MDR.haeHampurilainen();
		System.out.println(MBurgeri);

	}

}
