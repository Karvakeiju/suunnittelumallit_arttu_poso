package main.java.teht_14;

public class McDonaldsBuilder implements IHampurilaisBuilder {
	
	private McDonaldsBurger burgeri;

	@Override
	public void luoUusiBurgeri() {
		burgeri = new McDonaldsBurger();
		
	}

	@Override
	public void asetaLeivat() {
		burgeri.asetaLeipa(new Leipä("sämpylä"));
	}

	@Override
	public void lisaaAinekset() {
		burgeri.lisaaLisuke(new Lisuke("pihvi"));
		burgeri.lisaaLisuke(new Lisuke("salaatti"));
		burgeri.lisaaLisuke(new Lisuke("juusto"));
		burgeri.lisaaLisuke(new Lisuke("tomaatti"));
		
	}

	@Override
	public void asetaKastike() {
		burgeri.lisaaLisuke(new Lisuke("ketsuppi"));
		burgeri.lisaaLisuke(new Lisuke("sinappi"));
		
	}

	@Override
	public Object haeHampurilainen() {
		return burgeri;
	}

}

