package main.java.teht_14;

import java.util.ArrayList;
import java.util.List;

public class McDonaldsBurger {
	private Leipä leipa;
	private List<Lisuke> lisukkeet = new ArrayList<Lisuke>();
	
	public void asetaLeipa(Leipä leipa) {
		this.leipa = leipa;
	}
	
	public void lisaaLisuke(Lisuke lisuke) {
		lisukkeet.add(lisuke);
	}
	
	public String toString() {
		String merkkijono = "";
		if(leipa!=null)
			merkkijono+="Leipä: "+leipa.toString();
		if(lisukkeet.size()>0)
			merkkijono+=", Täytteet: ";
		for(int i = 0; i<lisukkeet.size(); i++) {
			merkkijono+=lisukkeet.get(i);
			if(i<lisukkeet.size()-1)
				merkkijono+=", ";
		}
		return merkkijono+".";
	}

}
