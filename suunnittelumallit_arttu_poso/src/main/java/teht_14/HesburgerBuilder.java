package main.java.teht_14;

public class HesburgerBuilder implements IHampurilaisBuilder {
	
	private HesburgerBurger burgeri;

	@Override
	public void luoUusiBurgeri() {
		burgeri = new HesburgerBurger();
		
	}

	@Override
	public void asetaLeivat() {
		burgeri.asetaLeipa("kerrosleipä");
		
	}

	@Override
	public void lisaaAinekset() {
		burgeri.asetaLisukkeet("juustoa, pihvit, suolakurkut");
		
	}

	@Override
	public void asetaKastike() {
		burgeri.asetaKastikkeet("ketsuppi, sinappi, paprikamajoneesi");
		
	}

	@Override
	public Object haeHampurilainen() {
		return burgeri;
	}

}
