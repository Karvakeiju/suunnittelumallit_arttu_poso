package main.java.teht_14;

public class Kokki {
	private IHampurilaisBuilder rakentaja;
	
	public Kokki() {
		rakentaja = null;
	}
	
	public Kokki(IHampurilaisBuilder hr) {
		rakentaja = hr;
	}
	
	public void asetaRakentaja(IHampurilaisBuilder hr) {
		rakentaja = hr;
	}
	
	public void rakennaBurgeri() {
		if(rakentaja == null)
			return;
		rakentaja.luoUusiBurgeri();
		rakentaja.asetaLeivat();
		rakentaja.lisaaAinekset();
		rakentaja.asetaKastike();
	}

}
