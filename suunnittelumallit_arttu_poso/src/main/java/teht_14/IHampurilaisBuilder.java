package main.java.teht_14;

public interface IHampurilaisBuilder {
	public abstract void luoUusiBurgeri();
	public abstract void asetaLeivat();
	public abstract void lisaaAinekset();
	public abstract void asetaKastike();
	public abstract Object haeHampurilainen();

}
