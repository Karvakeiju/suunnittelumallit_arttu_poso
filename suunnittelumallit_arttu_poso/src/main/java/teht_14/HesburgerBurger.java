package main.java.teht_14;

public class HesburgerBurger {
	private String leipa = "";
	private String lisukkeet = "";
	private String kastikkeet = "";
	
	public void asetaLeipa(String leipa) {
		this.leipa = leipa;
	}
	
	public void asetaLisukkeet(String lisukkeet) {
		this.lisukkeet = lisukkeet;
	}
	
	public void asetaKastikkeet(String kastikkeet) {
		this.kastikkeet = kastikkeet;
	}
	
	public String toString() {
		return "Leipä: "+leipa+", lisukkeet: "+lisukkeet+", kastikkeet: "+kastikkeet+".";
	}

}
