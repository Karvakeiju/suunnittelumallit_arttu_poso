package main.java.teht_18;

public class Main {

	public static void main(String[] args) {
		Kello kello = new Kello();
		
		System.out.println("--Kello 1 viisarit osoittaa:");
		System.out.println(kello);
		
		System.out.println("\n--Kasvatetaan kello 1:n aikaa 120 sekunnilla");
		for(int i = 0; i < 120; i++) {
			kello.kasvata();
		}
		System.out.println("--Kello 1 viisarit osoittaa:");
		System.out.println(kello);
		
		System.out.println("\n--Kasvatetaan kello 1:n aikaa 3600 sekunnilla");
		
		for(int i = 0; i < 3600; i++) {
			kello.kasvata();
		}
		System.out.println("--Kello 1 viisarit osoittaa:");
		System.out.println(kello);
		
		System.out.println("\n--Tehdään syväkopio kello 1:stä => kello 2");
		
		Kello kello2 = kello.clone();
		
		System.out.println("--Kello 2 viisarit osoittaa:");
		System.out.println(kello2);
		
		System.out.println("\n--Kasvatetaan kello 1:n aikaa 7200 sekunnilla");

		for(int i = 0; i < 7200; i++) {
			kello.kasvata();
		}
		System.out.println("--Kello 1 vs kello 2");
		System.out.println("Kello 1: " + kello);
		System.out.println("Kello 2: " + kello2);
		
		System.out.println("\n--Reset kello 1, eli kaikki viisarit osoittaa 0");
		kello.reset();
		System.out.println("--Kello 1 vs kello 2");
		System.out.println("Kello 1: " + kello);
		System.out.println("Kello 2: " + kello2);

	}
}
