package main.java.teht_01;

public class Normo extends AterioivaOtus {

	public Juoma createJuoma() {
		return new Limu();
	}

	public Ruoka createRuoka() {
		return new Pizza();
	}
}
