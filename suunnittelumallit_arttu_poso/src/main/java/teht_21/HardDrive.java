package main.java.teht_21;

import java.util.ArrayList;
import java.util.List;

public class HardDrive {
	private List<String> data;
	private final String bootCode = "konekieli joka ladataan RAM:iin";
	
	public HardDrive() {
		data = new ArrayList<String>();
		data.add(bootCode);
		
		try {
			//Tämän ei pitäisi olla ikinä false
			if(!data.get(0).equals(bootCode))
				data.set(0, bootCode);
		} catch (IndexOutOfBoundsException e) {
			System.err.println("Paha virhe");
		}
	}
	
	public String read(long lba, int size){
		System.out.println("-HARDDRIVE: luetaan tieto paikasta: "+lba+"\n      '"+data.get((int)lba)+"'");
		return data.get((int)lba);
	}
}
