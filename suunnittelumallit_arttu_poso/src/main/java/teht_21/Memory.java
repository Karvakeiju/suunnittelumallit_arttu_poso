package main.java.teht_21;

import java.util.ArrayList;
import java.util.List;

public class Memory {
	private List<String> data;
	
	public Memory() {
		data = new ArrayList<String>();
	}
	
	public void load(long position, String dataToAdd) {
		//Varmistetaan listan olevan tarpeeksi suuri, jotta tallettettava data mahtuu oikeean indeksiin.
		long diff = position+1 - data.size();
		//Lisätään tarpeeksi null-alkioita, jotta data saadaan oikeaan kohtaan.
		if(diff>0)
			for(long i = diff;i>0;i--)
				data.add(null);
		data.set((int)position, dataToAdd);
		System.out.println("-MEMORY: '"+dataToAdd+"' lisättiin paikkaan: "+position);
			
	}

}
